import Vue from 'vue'
import VueRouter from 'vue-router'
import GoodListView from '../views/GoodList.vue'
import ShopCartView from '../views/ShopCart.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'GoodList',
    component: GoodListView
  },
  {
    path: '/shopcart',
    name: 'ShopCart',
    component: ShopCartView
  }
 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
